import React from 'react';

import './Post.css';

const post = (props) => (
    <article className="Post" onClick={props.click}>
        <h1>{props.titulo}</h1>
        <div className="Info">
            <div className="Author">{props.autor}</div>
        </div>
    </article>
);

export default post;