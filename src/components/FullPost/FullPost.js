import React, { Component } from 'react';

import './FullPost.css';
import Axios from 'axios';

class FullPost extends Component {
    state = {
        leerPost: null
    }

    componentDidUpdate() {
        if (this.props.id) {
            if (!this.state.leerPost || (this.state.leerPost && this.state.leerPost.id !== this.props.id)) {
                Axios.get('/posts/' + this.props.id)
                    .then(respuesta => {
                        // console.log(respuesta)
                        this.setState({ leerPost: respuesta.data })
                    })
            }
        }

    }

    render() {
        let post = <p style={{ textAlign: 'center' }}>Please select a Post!</p>;
        if (this.props.id) {
            post = <p style={{ textAlign: 'center' }}>Cargando...!</p>;
        }
        if (this.state.leerPost) {
            post = (
                <div className="FullPost">
                    <h1>{this.state.leerPost.title}</h1>
                    {/*<p>Content</p>*/}
                    <p>{this.state.leerPost.body}</p>
                    <div className="Edit">
                        <button onClick={this.elimiarPost} className="Delete">Delete</button>
                    </div>
                </div>
            );
        }
        return post;
    }
    elimiarPost = () => {
        Axios.delete('/posts/' + this.props.id)
            .then(r =>{
                console.log(r)
            });
    }
    
}

export default FullPost;