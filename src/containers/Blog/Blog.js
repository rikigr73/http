import React, { Component } from 'react';

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import './Blog.css';
//import Axios from 'axios';
import AxiosInstancia from '../../axios';


class Blog extends Component {
    state={
        posts:[],
        seleccionarId: null,
        errores: false
    }

    componentDidMount(){
        AxiosInstancia.get('/posts')
            .then(respuesta =>{
                const posts = respuesta.data.slice(0,4);
                const actualizaPost = posts.map(r=>{
                    return {
                        ...r,
                        autor:'Ricardo'
                    }
                })
                this.setState({posts: actualizaPost})
                //console.log(respuesta);
            })
            .catch(error => {
                console.log(error);
                this.setState({errores: true})
            });
    }

    seleccionarPost = (id) => {
        this.setState({seleccionarId:id})
    }
    

    render () {
        let posts = <p style={{textAlign: 'center'}}>Algo salio mal</p>
        if(!this.state.errores){
            posts = this.state.posts.map(dato => {
                return <Post 
                titulo={dato.title} 
                key={dato.id} 
                autor={dato.autor}
                click={() => this.seleccionarPost(dato.id)}/>
        })
        }
            

        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <section>
                    <FullPost id={this.state.seleccionarId}/>
                </section>
                <section>
                    <NewPost />
                </section>
            </div>
        );
    }
}

export default Blog;