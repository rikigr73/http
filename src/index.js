import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Axios from 'axios';

//urs base
Axios.defaults.baseURL='https://jsonplaceholder.typicode.com'
Axios.defaults.headers.common['Authorization'] ='AUTH TOKEN'
Axios.defaults.headers.post['Content-Type'] = 'application/json'

Axios.interceptors.request.use(r=>{
    console.log(r)
    //siempre enviar el return de la consulta
    return r;
},error =>{
    console.log(error)
    return Promise.reject(error)
});


ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
