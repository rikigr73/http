import Axios from "axios";

const instancia = Axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
});

instancia.defaults.headers.common['Authorization'] ='AUTH TOKEN FROM INSTANCE'

export default instancia;